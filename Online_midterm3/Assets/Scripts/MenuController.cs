﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
  public Texture2D tex2D;

  void SetCursor()
  {
    CursorMode mode = CursorMode.ForceSoftware;
    int xSpot = tex2D.width / 2;
    int ySpot = tex2D.height / 2;
    Vector2 hotSpot = new Vector2(xSpot, ySpot);
    Cursor.SetCursor(tex2D,hotSpot,mode);
  }

  private void Start()
  {
    SetCursor();
  }

  public void OnClickCharacterPick(int whichCharacter)
  {
    if (PlayerInfo.PI != null)
    {
      PlayerInfo.PI.mySelectedCharacter = whichCharacter;
      PlayerPrefs.SetInt("MyCharacter",whichCharacter);
    }
  }
}
