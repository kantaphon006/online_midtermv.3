﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{ 
    public PhotonView PV;
    public float movementspeed;
    private Rigidbody myRb;

    public Camera myCamera;
    public Transform reticle;
    public Transform cameraTarget;

    
    void Start()
    {
        PV = GetComponent<PhotonView>();
        myRb = GetComponent<Rigidbody>();
        reticle.parent = null;
    }

    

    public void SetUp(Camera cameraIn)
    {
        myCamera = cameraIn;
    }

    // Update is called once per frame
    void Update()
    {
        if(!PV.IsMine)
            return;
        PCLookAtMouse();
    }

    

     void PCLookAtMouse()
     {
        Plane playerPlane = new Plane(Vector3.up, transform.position);
         Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
         float hitDist = 0.0f;

         if (playerPlane.Raycast(ray,out hitDist))
         {
             Vector3 targetPoint = ray.GetPoint(hitDist);
             Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            targetRotation.x = 0;
            targetRotation.z = 0;
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 7f * Time.deltaTime);
             
        }
     }
     void playerMovement()
     {
         if (Input.GetKey(KeyCode.W))
         {
             transform.Translate(Vector3.forward *movementspeed*Time.deltaTime);
         }
         if (Input.GetKey(KeyCode.A))
         {
             transform.Translate(Vector3.left *movementspeed*Time.deltaTime);
         }
         if (Input.GetKey(KeyCode.D))
         {
             transform.Translate(Vector3.right *movementspeed*Time.deltaTime);
         }
         if (Input.GetKey(KeyCode.S))
         {
             transform.Translate(Vector3.back *movementspeed*Time.deltaTime);
         }
     }

      void FixedUpdate()
     {
         if (PV.IsMine)
         {
             PCLookAtMouse();
             playerMovement();
         }
     }
}
