﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using  Photon.Realtime;
using UnityEngine;
using Random = UnityEngine.Random;


public class PhotonLobby : MonoBehaviourPunCallbacks
{
    public static PhotonLobby lobby;
    public int roomNumber;
    RoomInfo[] rooms;
    public GameObject StartBtn;
    public GameObject CancelBtn;
    private void Awake()
    {
        lobby = this;
    }

    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();//connect to Master Photon
    }
    
    public override void OnConnectedToMaster()
    {
        Debug.Log("Player has connect to the Photon master sever");
        PhotonNetwork.AutomaticallySyncScene = true;
        StartBtn.SetActive(true);
    }

    public void OnStartbtnClick()
    {
        Debug.Log("Start Button was Click");
        StartBtn.SetActive(false);
        CancelBtn.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
        
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to join a random game but Failed. There must be no open game available");
      CreateRoom();
    }

    void CreateRoom()
    {
        Debug.Log("Try to Create a new Room");
        int randomRoomName = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions() {IsVisible = true, IsOpen = true, MaxPlayers = 4};
        PhotonNetwork.CreateRoom("Room"+randomRoomName, roomOps);
    }

   

    
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create room but Failed. There must already ba a room with the same name");
        CreateRoom();
    }

    public void OnCancelBtnClick()
    {Debug.Log("Cancel Button was Click");
        CancelBtn.SetActive(false);
        StartBtn.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }
}


