﻿using System;
using System.IO;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class PunHealth : MonoBehaviourPunCallbacks , IPunObservable
{
    public const int maxHealth = 100;
    public static int currentHealth = maxHealth;
    public GameObject myAvatar;

    public static PunHealth Health;
    public Text HealthTxt;
     void Awake()
    {
        if (PunHealth.Health == null)
        {
            PunHealth.Health = this;
        }
    }

    void Start()
    {
        if (!photonView.IsMine)
        return;
        
     }

    public void OnGUI()
    {
        if (photonView.IsMine)
        {
            GUI.Label(new Rect(50, 50, 300, 162), "Player Health : " + currentHealth);
        }

    }

    public void TakeDamage(int amount, int OwnerNetID)
    {
        if (photonView != null)
            photonView.RPC("PunRPCTakedDamage", RpcTarget.All, amount, OwnerNetID);
        else print("photonView is NULL.");
    }

    [PunRPC]
    public void PunRPCTakedDamage(int amount, int OwnerNetID)
    {
        Debug.Log("Take Damage");
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            Debug.Log("NetID : " + OwnerNetID.ToString() + " Killed " + photonView.ViewID);
            photonView.RPC("PunResetPlayer", photonView.Owner);
        }
    }

    

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            stream.SendNext(currentHealth);
        }
        else {
            currentHealth = (int) stream.ReceiveNext();
        }
    }

    [PunRPC]
   public void PunResetPlayer()
   {
       int spawnPicker = Random.Range(0, GameSetup.GS.spawnPoints.Length);
       Debug.Log("Reset Position..");
       
        currentHealth = maxHealth;
   }

    void Update()
    {
        HealthTxt.text = currentHealth.ToString();
    }
}
