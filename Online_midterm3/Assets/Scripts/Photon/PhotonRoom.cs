﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using  System.IO;
using Photon.Pun;
using Photon.Realtime;

public class PhotonRoom : MonoBehaviourPunCallbacks,IInRoomCallbacks
{
    //RoomInfo
    public static PhotonRoom room;
    private PhotonView PV;

    //public bool isGameLoaded;
    public int currentScene;
    public int MultiPlayerScene;
    
    //PlayerInfo
    Player[] phontonPlayers;
   public int playersInRoom;
    public int myNumberInRoom;
   
   public int playerInGame;

    private void Awake()
    {
        if (PhotonRoom.room == null)
        {
            PhotonRoom.room = this;
        }
        else
        {
            if (PhotonRoom.room !=this)
                Destroy(PhotonRoom.room.gameObject) ;
            PhotonRoom.room = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
        //PhotonNetwork.CurrentRoom.IsOpen = false;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }

    

    void Start()
    {
        PV = GetComponent<PhotonView>();
    }
    void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        currentScene = scene.buildIndex;
        if (currentScene == MultiPlayerScene)
        {
            {
                CreatePlayer();
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("You are in a room");
        phontonPlayers = PhotonNetwork.PlayerList;
        playersInRoom = phontonPlayers.Length;
        myNumberInRoom = playersInRoom;
       PhotonNetwork.NickName = myNumberInRoom.ToString();
       {
           StartGame();
       }
    }

     void StartGame()
    {
        if(!PhotonNetwork.IsMasterClient)
            return;
        PhotonNetwork.LoadLevel(MultiPlayerScene);
    }

     private void CreatePlayer()
     {
         PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonNetworkPlayer"), transform.position,
             Quaternion.identity,0);
     }

     public override void OnPlayerLeftRoom(Player otherPlayer)
     {
         base.OnPlayerLeftRoom(otherPlayer);
         Debug.Log(otherPlayer.NickName+"has left the game");
         playerInGame--;
     }
}
