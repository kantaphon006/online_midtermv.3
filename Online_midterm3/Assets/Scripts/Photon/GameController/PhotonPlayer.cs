﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using UnityEngine;

public class PhotonPlayer : MonoBehaviour
{
    public PhotonView PV;
    public GameObject myAvatar;
    void Start()
    {
        PV = GetComponent<PhotonView>();
        int spawnPicker = Random.Range(0, GameSetup.GS.spawnPoints.Length);
        if (PV.IsMine)
        {
           myAvatar= PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerAvatar"),
                GameSetup.GS.spawnPoints[spawnPicker].position,
                GameSetup.GS.spawnPoints[spawnPicker].rotation, 0);
           myAvatar.GetComponent<PlayerMovement>().SetUp(PhotonGameSetUp.PGS.myCamera);
           PhotonGameSetUp.PGS.myCamera.GetComponent<CameraFollow>().target = myAvatar.GetComponent<PlayerMovement>().cameraTarget;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
