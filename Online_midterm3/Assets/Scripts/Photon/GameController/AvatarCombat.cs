﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class AvatarCombat : MonoBehaviourPun
{
    private PhotonView PV;
    private AvatarSetup avatarSetup;
    
    public GameObject BulletSpawnPoint;
    public Text healthDisplay;
    public float BulletForce = 20f;
    public GameObject Bullet;
    void Start()
    {
        PV = GetComponent<PhotonView>();
        avatarSetup = GetComponent<AvatarSetup>();
        healthDisplay = GameSetup.GS.healthDisplay;
    }

    // Update is called once per frame
    void Update()
    {
        if (!PV.IsMine)
            return;
        
        
        if (Input.GetMouseButtonDown(0))
        {
            PV.RPC("RPC_Shooting",RpcTarget.All);
        }

        
        //healthDisplay.text = PunHealth.currentHealth.ToString();
        
    }

    [PunRPC]
    void RPC_Shooting()
    {
        CmdFire(BulletSpawnPoint.transform.rotation);
    }
    void CmdFire(Quaternion rotation)
    {
        //Keep Data when Instantiate.
        object[] data = { PV.ViewID };

        // Spawn the bullet on the Clients and Create the Bullet from the Bullet Prefab
        Rigidbody bullet = PhotonNetwork.Instantiate(this.Bullet.name
            , this.transform.position + (this.transform.forward * 1.5f)
            , rotation
            , 0
            , data).GetComponent<Rigidbody>();

        // Add velocity to the bullet
        bullet.velocity = bullet.transform.forward * BulletForce;

        // Destroy the bullet after 10 seconds
        Destroy(bullet.gameObject, 10.0f);
    }

    
     
}
